﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KjsTcp.KjsTcpListener
{
    public class KjsTcpListener
    {

        string _heartBeatMessage;

        IPAddress _host;
        int _port;

        TcpListener _listener;
        TcpClient _client;

        Thread _listenerThread;
        Thread _CheckActiveThread;
        Thread _ReceiveDataThread;

        List<TcpClient> ConnectedClients { get; set; }
        public List<TcpClient> ActiveConnections { get; set; }

        public bool IsListening;

        public KjsTcpListener(IPAddress host, int port, string heartBeatMessage = " ")
        {
            _host = host;

            _port = port;

            _heartBeatMessage = String.IsNullOrWhiteSpace(heartBeatMessage) ? "Hello, World!" : heartBeatMessage;

            IsListening = false;
        }

        public KjsTcpListener(string host, int port, string heartBeatMessage = " ")
        {
            IPAddress.TryParse(host, out _host);

            _port = port;

            _heartBeatMessage = String.IsNullOrWhiteSpace(heartBeatMessage) ? "Hello, World!" : heartBeatMessage;

            IsListening = false;
        }

        private ManualResetEvent tcpClientConnected = new ManualResetEvent(false);

        #region ITcp

        public void Start()
        {
            try
            {
                Logging.WriteLine("Starting TcpListener host {0} port {1}", _host, _port);

                ActiveConnections = new List<TcpClient>();

                ConnectedClients = new List<TcpClient>();

                _CheckActiveThread = new Thread(CheckClientConnection)
                {
                    IsBackground = true,
                    Name = "Check Client Connection"
                };

                _CheckActiveThread.Start();

                IsListening = true;

                _listener = new TcpListener(_host, _port);
                _listener.Start();

                _listenerThread = new Thread(Listening)
                {
                    IsBackground = true,
                    Name = "Listening Thread"
                };
                _listenerThread.Start();

                _ReceiveDataThread = new Thread(CheckForData)
                {
                    IsBackground = true,
                    Name = "Receive Data Thread"
                };
                _ReceiveDataThread.Start();
            }
            catch (SocketException ex)
            {
                Logging.WriteLine("Unable to start TcpListener {0}", ex.Message);

                OnError(_listener, ex);
                throw;
            }
            
        }

        private void Listening()
        {
            while (IsListening)
            {
                tcpClientConnected.Reset();

                Logging.WriteLine("Waiting for a connection...");

                _listener.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), _listener);

                tcpClientConnected.WaitOne();
            }
        }

        private void CheckForData()
        {
            while (IsListening)
            {
                byte[] clientData;

                for (int i = ConnectedClients.Count - 1; i >= 0; i--)
                {
                    var clients = ConnectedClients[i];
                    clientData = new byte[1024];
                    Stream clientStream = clients.GetStream();
                    int dataLen = clientStream.Read(clientData, 0, clientData.Length);
                    if (dataLen > 0)
                    {
                        string message = ASCIIEncoding.ASCII.GetString(clientData).Replace("\0", string.Empty);
                        DataReceived(clients, message);
                    }
                }
                //foreach (var clients in ConnectedClients)
                //{
                //    //ToDo: make async
                //    clientData = new byte[1024];
                //    Stream clientStream = clients.GetStream();
                //    int dataLen = clientStream.Read(clientData, 0, clientData.Length);
                //    if (dataLen > 0)
                //    {
                //        string message = ASCIIEncoding.ASCII.GetString(clientData).Replace("\0", string.Empty);
                //        DataReceived(clients, message);
                //    }
                //}
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
            
        }

        private void AcceptClient(IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;

            _client = listener.EndAcceptTcpClient(ar);

            Logging.WriteLine("Client {0} connected", _client.Client.RemoteEndPoint);

            ConnectedClients.Add(_client);

            tcpClientConnected.Set();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        private void CheckClientConnection()
        {
            List<TcpClient> disconnectedClients;
            while (IsListening)
            {
                Logging.WriteLine("Checking client connection");

                disconnectedClients = new List<TcpClient>();
                foreach (var client in ConnectedClients)
                {
                    try
                    {
                        byte[] message = ASCIIEncoding.ASCII.GetBytes(_heartBeatMessage);
                        Stream clientStream = client.GetStream();
                        clientStream.Write(message, 0, message.Length);
                    }
                    catch (Exception ex)
                    {
                        Logging.WriteLine("Connection to {0} lost in heatbeat", client.Client.RemoteEndPoint);
                        OnError(client, ex);
                    }
                }

                //Remove disconnected clients from ConnectedClients
                ConnectedClients.RemoveAll(x => x.Connected == false);
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~KjsTcpListener() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #endregion

        #region Events

        public event EventHandler<Exception> Error;
        protected virtual void OnError(object sender, Exception e)
        {
            Error?.Invoke(sender, e);
        }

        public event EventHandler<string> DataReceived;
        protected virtual void OnDataReceived(object sender, string data)
        {
            DataReceived?.Invoke(sender, data);
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KjsTcp
{
    public interface ITcp<T> : IDisposable
    {
        List<T> ActiveConnections { get; set; }

        void Start();

        void Listening();
    }
}

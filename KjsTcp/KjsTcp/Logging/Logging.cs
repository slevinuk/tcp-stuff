﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KjsTcp
{
    class Logging
    {
        public static string LogTitle { get; set; }

        public static void WriteLine(string message)
        {
            Trace.WriteLine(message);
        }

        public static void WriteLine(string message, params object[] arg0)
        {
            if (arg0 == null)
            {
                WriteLine(message);
                return;
            }
                
            WriteLine(string.Format(message, arg0));
        }
    }
}

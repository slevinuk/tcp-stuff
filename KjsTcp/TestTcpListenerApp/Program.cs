﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KjsTcp.KjsTcpListener;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.IO;

namespace TestTcpListenerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            KjsTcpListener listener = new KjsTcpListener(IPAddress.Any, 1234, "Hello World");
            listener.Start();

            listener.DataReceived += Listener_DataReceived;
            listener.Error += Listener_Error;

            //Thread.Sleep(TimeSpan.FromSeconds(5));

            //TcpClient client = new TcpClient();
            //client.Connect("192.168.0.32", 1234);

            //Stream clientStream = client.GetStream();
            //string message = "This is kevin";
            //byte[] clientdata = ASCIIEncoding.ASCII.GetBytes(message);
            //clientStream.Write(clientdata, 0, clientdata.Length);

            Console.ReadKey();
        }

        private static void Listener_Error(object sender, Exception e)
        {
            Console.WriteLine(e.InnerException);
        }

        private static void Listener_DataReceived(object sender, string e)
        {
            Console.Write(e);
        }
    }
}
